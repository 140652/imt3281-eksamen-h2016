package no.ntnu.imt3281.starWars;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FetchFromWWWTest {

	@Test
	public void filmConstructorTest() {
		Film film = new Film("films/2/");
		assertEquals("The Empire Strikes Back", film.getTitle());
	}
	
	@Test 
	public void peopleConstructorTest() {
		People character = new People("people/1/");
		assertEquals("Luke Skywalker", character.getName());
	}
	
	@Test
	public void planetConstructorTest() {
		Planet planet = new Planet("planets/1/");
		assertEquals("Tatooine", planet.getName());
	}
	
	@Test 
	public void speciesConstructorTest() {
		Species species = new Species("species/1/");
		assertEquals("Human", species.getName());
	}
	
	@Test
	public void starshipConstructorTest() {
		Starship starship = new Starship("starships/2/");
		assertEquals("CR90 corvette", starship.getName());
	}
	
	@Test
	public void vehicleConstructorTest() {
		Vehicle vehicle = new Vehicle("vehicles/7/");
		assertEquals("X-34 landspeeder", vehicle.getName());
	}
}
