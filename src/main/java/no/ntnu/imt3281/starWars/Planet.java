/**
 * 
 */
package no.ntnu.imt3281.starWars;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import no.ntnu.imt3281.eksamen.DatabaseController;
import no.ntnu.imt3281.eksamen.W3ApiController;

/**
 * @author amund
 * Class holds information about a planet.
 */
public class Planet {

	private String id;
	private String name;
	private int rotationPeriod;
	private int orbitalPeriod;
	private int diameter;
	private String climate;
	private String gravity;
	private String terrain;
	private int surfaceWater;
	private long population;
	private ArrayList<String> residents;
	private ArrayList<String> films;

	/**
	 * Constructor takes an id and a json string with the data about a planet. 
	 * @param id for the planet
	 * @param json string with data about the planet 
	 */
	public Planet(String id, String json) {
		this.id = id;
		parseJson(json);

		DatabaseController.getController().addPlanet(this);

	}

	/**
	 * @param json
	 */
	private void parseJson(String json) {
		JSONParser parser = new JSONParser();

		try {
			JSONObject object = (JSONObject) parser.parse(json);

			name = object.get("name").toString();
			rotationPeriod = Integer.parseInt((String)object.get("rotation_period"));
			orbitalPeriod = Integer.parseInt((String)object.get("orbital_period"));
			diameter = Integer.parseInt((String)object.get("diameter"));
			climate = (String) object.get("climate");
			gravity = (String) object.get("gravity");
			terrain = (String) object.get("terrain");
			
			if(object.get("surface_water").toString().equals("unknown")){
				surfaceWater = -1;
			}
			else{
				surfaceWater = Integer.parseInt((String) object.get("surface_water"));
			}
			
			if(object.get("population").toString().equals("unknown")){
				population = -1;
			}
			else{
				population = Long.parseLong((String) object.get("population"));
			}
			
			residents = null;
			JSONArray resArr = (JSONArray) object.get("residents");
			if(resArr != null){
				residents = new ArrayList<>();
				Iterator<String> itr = resArr.iterator();
				while(itr.hasNext()){
					residents.add(itr.next().substring(20));
				}
			}

			films = null;
			JSONArray filmsArr = (JSONArray) object.get("films");
			if(filmsArr != null){
				films = new ArrayList<>();
				Iterator<String> itr = filmsArr.iterator();
				while(itr.hasNext()){
					films.add(itr.next().substring(20));
				}
			}

		} catch (ParseException e) {
			System.err.println("Error parsing planet json\n");
			e.printStackTrace();
		}
	}

	/**
	 * Looks for object in database, or fetches from swapi.co/api/
	 * @param id for the planet
	 */
	public Planet(String id) {
		Object[] dbData = DatabaseController.getController().findObjectById(id);
		if(dbData == null){
			System.out.println("Planet not in db\n");
			
			this.id = id;
			parseJson(W3ApiController.getW3apicontroller().getJsonForId(id));
			
			DatabaseController.getController().addPlanet(this);
		}
		else{
			this.id = ((String) dbData[0]).trim();
			name = (String) dbData[1];
			rotationPeriod = (int) dbData[2];
			orbitalPeriod = (int) dbData[3];
			diameter = (int) dbData[4];
			climate = (String) dbData[5];
			gravity = (String) dbData[6];
			terrain = (String) dbData[7];
			surfaceWater = (int) dbData[8];
			population = (long) dbData[9];
			
			// spilts strings on , and adds them to arraylists
			residents = new ArrayList<>();
			String[] resIds = dbData[7].toString().split(",");
			for(String resId : resIds){
				residents.add(resId);
			}

			films = new ArrayList<>();
			String[] filmsIds = dbData[10].toString().split(",");
			for(String filmId : filmsIds){
				films.add(filmId);
			}

		}

	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the rotationPeriod
	 */
	public int getRotationPeriod() {
		return rotationPeriod;
	}

	/**
	 * @return the orbitalPeriod
	 */
	public int getOrbitalPeriod() {
		return orbitalPeriod;
	}

	/**
	 * @return the diameter
	 */
	public int getDiameter() {
		return diameter;
	}

	/**
	 * @return the climate
	 */
	public String getClimate() {
		return climate;
	}

	/**
	 * @return the gravity
	 */
	public String getGravity() {
		return gravity;
	}

	/**
	 * @return the terrain
	 */
	public String getTerrain() {
		return terrain;
	}

	/**
	 * @return the surfaceWater
	 */
	public int getSurfaceWater() {
		return surfaceWater;
	}

	/**
	 * @return the population
	 */
	public long getPopulation() {
		return population;
	}

	/**
	 * @return a People ArrayList
	 */
	public ArrayList<People> getResidents() {
		ArrayList<People> ret = new ArrayList<>();
		for(String res : residents){
			ret.add(new People(res));
		}

		return ret;
	}

	/**
	 * @return the residents ids
	 */
	public ArrayList<String> getResidentsIds() {
		return residents;
	}

	/**
	 * Gets the ids of the Films
	 */
	public ArrayList<String> getFilmsIds(){
		return films; 
	}

	/**
	 * @return a Films ArrayList
	 */
	public ArrayList<Film> getFilms() {
		ArrayList<Film> ret = new ArrayList<>();
		for(String film : films){
			ret.add(new Film(film));
		}

		return ret;
	}

	/**
	 * Overrides the toString method from object. Returns name. 
	 */
	@Override
	public String toString(){
		return getName();
	}


	/**
	 * Checks if an object is equal to this planet
	 * @param candidate the object to be compared
	 * @return true if the ids are equal
	 */
	@Override 
	public boolean equals(Object candiadate){

		if(candiadate instanceof Planet){
			Planet c = (Planet) candiadate;
			return c.getId().equals(getId());
		}
		return false;
	}
}
