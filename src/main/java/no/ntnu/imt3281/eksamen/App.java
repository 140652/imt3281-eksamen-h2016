package no.ntnu.imt3281.eksamen;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Hello world!
 *
 */
public class App extends Application {
	
	@SuppressWarnings("unused")
	private StarWars starWarsController;
	
	
	/**
	 * Overrides the Application start method, and initializes the Ludo-scene
	 */
	@Override
	public void start(Stage primaryStage) {
		// Initializes singletons
		DatabaseController.getController();
		W3ApiController.getW3apicontroller();
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../starWars/StarWars.fxml"));
			AnchorPane root = loader.load();
			starWarsController = loader.getController();
			
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			System.err.println("Error opening app\n");
			e.printStackTrace();
		}
		
	}
	
    public static void main( String[] args )
    {
    	launch(args);
        System.out.println( "Hello World!" );
        // Initiates the DatabaseController singleton
        DatabaseController.getController();
    }
}
