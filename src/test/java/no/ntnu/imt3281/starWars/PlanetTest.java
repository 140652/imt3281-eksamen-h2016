package no.ntnu.imt3281.starWars;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class PlanetTest {
	String json = "{"
			+"	\"name\": \"Alderaan\","
			+"	\"rotation_period\": \"24\","
			+"	\"orbital_period\": \"364\","
			+"	\"diameter\": \"12500\","
			+"	\"climate\": \"temperate\","
			+"	\"gravity\": \"1 standard\","
			+"	\"terrain\": \"grasslands, mountains\","
			+"	\"surface_water\": \"40\","
			+"	\"population\": \"2000000000\","
			+"	\"residents\": ["
			+"		\"http://swapi.co/api/people/5/\","
			+"		\"http://swapi.co/api/people/68/\","
			+"		\"http://swapi.co/api/people/81/\""
			+"	],"
			+"	\"films\": ["
			+"		\"http://swapi.co/api/films/6/\","
			+"		\"http://swapi.co/api/films/1/\""
			+"	],"
			+"	\"created\": \"2014-12-10T11:35:48.479000Z\","
			+"	\"edited\": \"2014-12-20T20:58:18.420000Z\","
			+"	\"url\": \"http://swapi.co/api/planets/2/\""
			+"}";

	@Test
	public void testConstructors() {
		// Create object with id and json string
		// Should create/update record in database.
		Planet planet = new Planet("planets/2/", json);
		assertEquals(planet.getId(), "planets/2/");
		assertEquals(planet.getName(), "Alderaan");
		assertEquals(planet.getName(), planet.toString());
		assertEquals(planet.getRotationPeriod(), 24, 0);
		assertEquals(planet.getOrbitalPeriod(), 364, 0);
		assertEquals(planet.getDiameter(), 12500, 0);
		assertEquals(planet.getClimate(), "temperate");
		assertEquals(planet.getGravity(), "1 standard");
		assertEquals(planet.getTerrain(), "grasslands, mountains");
		assertEquals(planet.getSurfaceWater(), 40, 0);
		assertEquals(planet.getPopulation(), 2000000000, 0);
		assertTrue(planet.getResidents() instanceof ArrayList<?>);
		assertTrue(planet.getResidents().get(0) instanceof People);
		assertEquals(planet.getResidents().size(), 3, 0);
		assertTrue(planet.getFilms() instanceof ArrayList<?>);
		assertTrue(planet.getFilms().get(0) instanceof Film);
		assertEquals(planet.getFilms().size(), 2, 0);

		// Create object with id only.
		// Should read record from database if exists or from 
		// network if not exists. Since we just added it above it should be in the database.
		Planet alderaan = new Planet("planets/2/");	// ID and json string
		assertEquals(planet, alderaan);				// Must override equals in Planet
	}
}