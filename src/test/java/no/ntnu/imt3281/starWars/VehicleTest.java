package no.ntnu.imt3281.starWars;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class VehicleTest {
	String json = "{"
			+"	\"name\": \"Snowspeeder\","
			+"	\"model\": \"t-47 airspeeder\","
			+"	\"manufacturer\": \"Incom corporation\","
			+"	\"cost_in_credits\": \"unknown\","
			+"	\"length\": \"4.5\","
			+"	\"max_atmosphering_speed\": \"650\","
			+"	\"crew\": \"2\","
			+"	\"passengers\": \"0\","
			+"	\"cargo_capacity\": \"10\","
			+"	\"consumables\": \"none\","
			+"	\"vehicle_class\": \"airspeeder\","
			+"	\"pilots\": ["
			+"		\"http://swapi.co/api/people/1/\","
			+"		\"http://swapi.co/api/people/18/\""
			+"	],"
			+"	\"films\": ["
			+"		\"http://swapi.co/api/films/2/\""
			+"	],"
			+"	\"created\": \"2014-12-15T12:22:12Z\","
			+"	\"edited\": \"2014-12-22T18:21:15.623033Z\","
			+"	\"url\": \"http://swapi.co/api/vehicles/14/\""
			+"}";
	
	@Test
	public void testConstructors() {
		// Create object with id and json string
		// Should create/update record in database.
		Vehicle vehicle = new Vehicle("vehicles/14/", json);
		assertEquals(vehicle.getId(), "vehicles/14/");
		assertEquals(vehicle.getName(), "Snowspeeder");
		assertEquals(vehicle.getName(), vehicle.toString());
		assertEquals(vehicle.getModel(), "t-47 airspeeder");
		assertEquals(vehicle.getManufacturer(), "Incom corporation");
		assertEquals(vehicle.getCost(), -1, 0);		// -1 for unknown
		assertEquals(vehicle.getLength(), 4.5, 0);
		assertEquals(vehicle.getMaxAtmospheringSpeed(), "650");
		assertEquals(vehicle.getCrew(), 2, 0);
		assertEquals(vehicle.getPassengers(), 0, 0);
		assertEquals(vehicle.getCargoCapacity(), 10, 0);
		assertEquals(vehicle.getConsumables(), "none");
		assertEquals(vehicle.getVehicleClass(), "airspeeder");
		assertTrue(vehicle.getPilots() instanceof ArrayList<?>);
		assertTrue(vehicle.getPilots().get(0) instanceof People);
		assertEquals(vehicle.getPilots().size(), 2, 0);
		assertTrue(vehicle.getFilms() instanceof ArrayList<?>);
		assertTrue(vehicle.getFilms().get(0) instanceof Film);
		assertEquals(vehicle.getFilms().size(), 1, 0);
		
		// Create object with id only.
		// Should read record from database if exists or from 
		// network if not exists. Since we just added it above it should be in the database.
		Vehicle snowspeeder= new Vehicle("vehicles/14/");	// ID and json string
		assertEquals(vehicle, snowspeeder);					// Must override equals in vehicle
	}
}
