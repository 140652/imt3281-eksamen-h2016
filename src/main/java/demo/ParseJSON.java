package demo;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;

import org.json.simple.*;
import org.json.simple.parser.*;


public class ParseJSON {
	public static void main(String[] args) {
		try {
	    	// The API at swapi.co is meant to be machine friendly. Returns data as json
	    	JSONParser parser = new JSONParser();
	    	// Can query films/people/planet/species/starships/vehicles
	    	URL url = new URL ("http://swapi.co/api/films/1/");
	    	URLConnection connection = url.openConnection();				// Need a bit extra
	    	// Must set user-agent, the java default user agent is denied
	    	connection.setRequestProperty("User-Agent", "curl/7.8 (i386-redhat-linux-gnu) libcurl 7.8 (OpenSSL 0.9.6b) (ipv6 enabled)");
	    	// Must set accept to application/json, if not html is returned
	    	connection.setRequestProperty("Accept", "application/json");
	    	connection.connect();

	    	// Read and parse the response
	    	JSONObject obj = (JSONObject) parser.parse(new InputStreamReader(connection.getInputStream()));
	    	// Get a single item from the JSON object
	    	System.out.println (obj.get("opening_crawl"));
	    	// Get an array from the JSON object
	    	JSONArray characters = (JSONArray) obj.get("characters");
	    	Iterator<String> iterator = characters.iterator();
	    	while (iterator.hasNext()) {				// Iterate over the array
	    		System.out.println(iterator.next());	// Print out each item
	    	}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
