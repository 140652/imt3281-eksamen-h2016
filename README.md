# 140652 #

# Svar på oppgavene #

Koden du skriver leveres med vanlige commit, husk minimum en commit pr. time utført arbeid med commit message som sier noe om hva du har gjort. 

Kommentarer til oppgavene og antakelser du gjør skriver du i denne filen.

Dersom du har spørsmål så bruker du "Issues" på det originale repositoriet, alle spørsmål og svar vil da bli samlet der.


Tekst: 

Antakelse: Constructor med id kjøres først. Sjekker databasen. Henter fra nett om den ikke er i db. 

Jeg har hardkodet alle keys tl json-stringene. Disse burde vært konstanter i objektet, men jeg lot dette være da de brukes et fåtall ganger. 

Alle objekter lager nå egne kopier av objekter som henter fra databasen eller fra nett når get*^[Ids]() kjøres. Disse brukes dog kun av testene, og jeg har derfor ikke bruke mer energi på det. Tanken er at StarWars.java holder alle data som ligger i minnet.

Antakelse: Data skal ikke lastes før brukeren ønsker å se de, så jeg har valgt å lese data fra nett i mange metoder i StarWars.java. "Films" sin json blir lest inn når W3ApiController lastes for første gang, da denne blir tatt i bruk først og ofte. Andre strenger som hentes av getTypeJson kan med fordel lagres. 

Angående oppgave 6: 
Data som hentes fra json endres enten til verdien -1, eller forblir unknown/none i strengformat. Disse ecapes på veg ut av databasen og på veg til visning hos brukeren. 

Om oppgave 7: 
For å vise data legger jeg til en onAction-metode fra controlleren som hører til hver velgbare combobox. 
Når brukeren gjør et valg leter jeg etter objektet. Rekkefølgen er ArrayList fra StarWars.java som ligger i minnet, databasen, nettapi. Når objektet med gitt navn er funnet (og eventuelt lagt i database) vises det i sin respektive fane. 
Ved utvidelse ville jeg lagt same listeners til alle comboboxer som refererer til samme type objekt. Feks alle People/Characters/Residents håndtert av loadCharacter-metoden.

Oppgave 9:
Lagre bildet vs lagre url:

Ved å lagre bildet på maskinen som kjører vil bildet kunne vises uten internettilgang. Karakterer som får bilde vist vil allerede ligge i databasen, så dette kan være hensiktsmessig. Det vil dog ta betraktelig mer plass enn å lagre url i databasen. 

Ved å lagre gyldige url'er i databasen (altså kun urler vi har brukt tidligere) kan vi benytte oss av at vi sikkert veit at et bilde finnes (gitt at det ikke er slettet fra wikia). Dette vil ta lite plass, og hvis vi antar at en bruker vil være koblet til nettet, burde ikke lasting av bilde ta lang tid. 

På bakgrunn av at dette i hovedsak er en applikasjon som kobler seg mot nettet, og at lagring av fjerneoppnåelig ("remote accessible") data er dårlig bruk av en brukers diskplass, velger jeg å kun lagre url i databasen. Dette gjøres veed hjelp av HTMlParser sin main-metode. Denne må kjøres før applikasjonen hvis urler skal lagres i databasen. 

Jeg har kun implementert dette for tabellen "people". De andre tabellene vil altereres og legges inn i på samme måte.


Oppgave 10: 
Se punktet "om oppgave 7". 
Ny kode som vil kreves for å vise data i alle tabs:

Kontroller-metoder som loadCharacter som koples til comboboxer med tilhørende typers id for planets, starships, species og vehicles. Disse ville byttet til riktig tab, og leitet etter/opprettet objektene som skal vises. 

ArrayLists<?> der ? representerer de siste 4 typene (slik som f.eks. loadedPeople). Kontrolleren ville lagt inn karakterer som lastes til minnet her. Slik loadPeople gjør nå.

show-metoder for de siste 4 typene. Lik showCharacter, tar et objekt og viser medlemsdataen. Noen av disse medlemsdataene kan ha ugyldige verdier, så disse må sjekkes slik det er gjort i showPeople (med f.eks. == eller .equals)

metodene addPictureUrlColumnToDatabase, updateIdWithUrl og getUrlFromId er nødt til å endres for å gjøre rom for url i andre tabeller. 

Forbedringer for å begrense kodeduplisering: 
Parsing av en JSONArray til en ArraList<String> kan trekkes ut av samtlige typer i StarWars-pakken. Disse ville jeg lagt i en funksjon i W3ApiController (da den har mest med json å gjøre generelt). Funksjonen ville tatt ArrayList<SString> og en JSONArray, og returnert en ferdig fylt ArrayList<String>. 




