/**
 * 
 */
package no.ntnu.imt3281.eksamen;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import no.ntnu.imt3281.starWars.Film;
import no.ntnu.imt3281.starWars.People;
import no.ntnu.imt3281.starWars.Planet;
import no.ntnu.imt3281.starWars.Species;
import no.ntnu.imt3281.starWars.Starship;
import no.ntnu.imt3281.starWars.Vehicle;

/**
 * @author amund
 * Singleton class. Connects to the database and handles queries. 
 */
public class DatabaseController {

	@SuppressWarnings("unused")
	private static DatabaseController databaseController = new DatabaseController();

	private static final String URL = "jdbc:derby:StarWarsDB";
	private Connection con;



	/**
	 * Constructor creates a connection to the database. 
	 */
	private DatabaseController(){
		try {
			con = DriverManager.getConnection(URL);
			System.out.println("Database connected\n");
		} catch (SQLException e) {
			System.err.println("Error connecting to database\n");
			e.printStackTrace();
		}
	}


	/**
	 * Gets the singleton object. 
	 * @return the DatabaseController object
	 */
	public static DatabaseController getController(){
		return databaseController;
	}


	/**
	 * Searches for an object with the given id. 
	 * Creates an object[] with the data found. 
	 * @param id of the object
	 * @return an object[] with the data in the order the database has stored the data, 
	 * 	or null if object was not found or error searching db
	 */
	public Object[] findObjectById(String id){
		// Gets the first part of the id (the table name and the type of the object)
		String table = id.toLowerCase().split("/")[0];
		if(table.endsWith("s") && !table.equals("species")){
			table = table.substring(0, table.length()-1);
		}
		Object[] data = null;

		try {
			PreparedStatement pstmt = con.prepareStatement("SELECT * FROM " + table + " "
					+ "WHERE id=?");
			pstmt.setString(1, id);

			ResultSet result = pstmt.executeQuery();

			// if there was a result, creates an arraylist with all the necessary data 
			if(result.next()){
				data = new Object[result.getMetaData().getColumnCount()];
				for(int i = 0; i < result.getMetaData().getColumnCount(); i++){
					data[i] = result.getObject(i+1);
				}
			}
			
			pstmt.close();
			
		} catch (SQLException e) {
			System.err.println("Error selecting from database\n");
			e.printStackTrace();
		}
		
		return data;
	}

	/**
	 * Tries to add a People object to the database. 
	 */
	public void addPeopleToDatabase(People people){
		PreparedStatement pstmt;
		try {
			if(findObjectById(people.getId()) != null){
				System.out.println("User is already in db. Updating.\n");
				pstmt = con.prepareStatement("UPDATE people "
						+ "SET id=?, name=?, height=?, mass=?, hairColor=?, skinColor=?, eyeColor=?, "
						+ "birthYear=?, gender=?, homeworld=?, films=?, species=?, vehicles=?, starships=?"
						+ "WHERE id=?");
				pstmt.setString(15, people.getId());
			}

			else{
				pstmt = con.prepareStatement("INSERT INTO people "
						+ "(id, name, height, mass, hairColor, skinColor, eyeColor, "
						+ "birthYear, gender, homeworld, films, species, vehicles, starships)"
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
			}

			pstmt.setString(1, people.getId());
			pstmt.setString(2, people.getName());
			pstmt.setString(3, Integer.toString(people.getHeight()));
			pstmt.setString(4, Double.toString(people.getMass()));
			pstmt.setString(5, people.getHairColor());
			pstmt.setString(6, people.getSkinColor());
			pstmt.setString(7, people.getEyeColor());
			pstmt.setString(8, people.getBirthYear());
			pstmt.setString(9, people.getGender());

			pstmt.setString(10, people.getHomeworldId());

			// adds array values separated by commas
			StringBuilder sb = new StringBuilder("");
			if(! people.getFilmsIds().isEmpty()){
				for(String film : people.getFilmsIds()) {
					sb.append(film);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(11, sb.toString());

			sb = new StringBuilder("");
			if(! people.getSpeciesIds().isEmpty()){
				for(String species : people.getSpeciesIds()) {
					sb.append(species + ',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(12, sb.toString());

			sb = new StringBuilder("");
			if(! people.getVehiclesIds().isEmpty()){
				for(String vehicle : people.getVehiclesIds()){
					sb.append(vehicle + ',');
				}
				sb.deleteCharAt(sb.length()-1);		
			}
			pstmt.setString(13, sb.toString());

			sb = new StringBuilder("");
			if(! people.getStarshipsIds().isEmpty()){
				for(String starship : people.getStarshipsIds()){
					sb.append(starship + ',');
				}
				sb.deleteCharAt(sb.length()-1);			
			}
			pstmt.setString(14, sb.toString());

			pstmt.execute();

			pstmt.close();

		} catch (SQLException e) {
			System.err.println("Error adding People to database\n");
			e.printStackTrace();
		}
	}

	/**
	 * Adds a film object correctly to the db
	 * @param film
	 */
	public void addFilm(Film film) {
		PreparedStatement pstmt;
		try {
			if(findObjectById(film.getId()) != null){
				System.out.println("Film is already in db. Updating.\n");
				pstmt = con.prepareStatement("UPDATE film "
						+ "SET id=?, title=?, episodeId=?, openingCrawl=?, director=?, producer=?, releaseDate=?, "
						+ "characters=?, planets=?, starships=?, vehicles=?, species=?"
						+ "WHERE id=?");
				pstmt.setString(13, film.getId());
			}

			else{
				pstmt = con.prepareStatement("INSERT INTO film "
						+ "(id, title, episodeId, openingCrawl, director, producer, releaseDate, "
						+ "characters, planets, starships, vehicles, species)"
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
			}

			pstmt.setString(1, film.getId());
			pstmt.setString(2, film.getTitle());
			pstmt.setString(3, Integer.toString(film.getEpisodeId()));
			pstmt.setString(4, film.getOpeningCrawl());
			pstmt.setString(5, film.getDirector());
			pstmt.setString(6, film.getProducer());
			pstmt.setString(7, film.getReleaseDate());

			// adds array values separated by commas
			StringBuilder sb = new StringBuilder("");
			if(! film.getCharactersIds().isEmpty()){
				for(String character : film.getCharactersIds()) {
					sb.append(character);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(8, sb.toString());

			sb = new StringBuilder("");
			if(! film.getPlanetsIds().isEmpty()){
				for(String planet : film.getPlanetsIds()) {
					sb.append(planet);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(9, sb.toString());

			sb = new StringBuilder("");
			if(! film.getStarshipsIds().isEmpty()){
				for(String starship : film.getStarshipsIds()) {
					sb.append(starship);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(10, sb.toString());

			sb = new StringBuilder("");
			if(! film.getVehiclesIds().isEmpty()){
				for(String vehicle : film.getVehiclesIds()) {
					sb.append(vehicle);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(11, sb.toString());

			sb = new StringBuilder("");
			if(! film.getSpeciesIds().isEmpty()){
				for(String specie : film.getSpeciesIds()) {
					sb.append(specie);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(12, sb.toString());

			pstmt.execute();

			pstmt.close();

		} catch (SQLException e) {
			System.err.println("Error adding Film to database\n");
			e.printStackTrace();
		}
	}

	
	/**
	 * Adds a planet-object correctly to database.
	 * @param planet
	 */
	public void addPlanet(Planet planet) {
		PreparedStatement pstmt;
		try {
			if(findObjectById(planet.getId()) != null){
				System.out.println("Planet is already in db. Updating.\n");
				pstmt = con.prepareStatement("UPDATE planet "
						+ "SET id=?, name=?, rotationPeriod=?, orbitalPeriod=?, diameter=?, climate=?,"
						+ "gravity=?, terrain=?, surfaceWater=?, population=?, residents=?, films=?"
						+ "WHERE id=?");
				pstmt.setString(13, planet.getId());
			}

			else{
				pstmt = con.prepareStatement("INSERT INTO planet "
						+ "(id, name, rotationPeriod, orbitalPeriod, diameter, climate, "
						+ "gravity, terrain, surfaceWater, population, residents, films)"
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
			}

			pstmt.setString(1, planet.getId());
			pstmt.setString(2, planet.getName());
			pstmt.setString(3, Integer.toString(planet.getRotationPeriod()));
			pstmt.setString(4, Integer.toString(planet.getOrbitalPeriod()));
			pstmt.setString(5, Integer.toString(planet.getDiameter()));
			pstmt.setString(6, planet.getClimate());
			pstmt.setString(7, planet.getGravity());
			pstmt.setString(8, planet.getTerrain());
			pstmt.setString(9, Integer.toString(planet.getSurfaceWater()));
			pstmt.setString(10, Long.toString(planet.getPopulation()));

			// adds array values separated by commas
			StringBuilder sb = new StringBuilder("");
			if(! planet.getResidentsIds().isEmpty()){
				for(String res : planet.getResidentsIds()) {
					sb.append(res);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(11, sb.toString());

			sb = new StringBuilder("");
			if(! planet.getFilmsIds().isEmpty()){
				for(String film : planet.getFilmsIds()) {
					sb.append(film);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(12, sb.toString());

			pstmt.execute();

			pstmt.close();

		} catch (SQLException e) {
			System.err.println("Error adding planet to database\n");
			e.printStackTrace();
		}


	}

	
	/**
	 * Adds a starship-object correctly to database.
	 * @param starship
	 */
	public void addStarship(Starship starship) {
		PreparedStatement pstmt;
		try {
			if(findObjectById(starship.getId()) != null){
				System.out.println("starship is already in db. Updating.\n");
				pstmt = con.prepareStatement("UPDATE starship "
						+ "SET id=?, name=?, model=?, manufacturer=?, cost=?, length=?,"
						+ "maxAtmospheringSpeed=?, crew=?, passengers=?, cargoCapacity=?, consumables=?, hyperdriveRating=?,"
						+ "MGLT=?, starshipClass=?, pilots=?, films=?"
						+ "WHERE id=?");
				pstmt.setString(17, starship.getId());
			}

			else{
				pstmt = con.prepareStatement("INSERT INTO starship "
						+ "(id, name, model, manufacturer, cost, length,"
						+ "maxAtmospheringSpeed, crew, passengers, cargoCapacity, consumables, hyperdriveRating,"
						+ "MGLT, starshipClass, pilots, films)"
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
			}

			pstmt.setString(1, starship.getId());
			pstmt.setString(2, starship.getName());
			pstmt.setString(3, starship.getModel());
			pstmt.setString(4, starship.getManufacturer());
			pstmt.setString(5, Double.toString(starship.getCost()));
			pstmt.setString(6, Float.toString(starship.getLength()));
			pstmt.setString(7, starship.getMaxAtmospheringSpeed());
			pstmt.setString(8, Integer.toString(starship.getCrew()));
			pstmt.setString(9, Integer.toString(starship.getPassengers()));
			pstmt.setString(10, Long.toString(starship.getCargoCapacity()));
			pstmt.setString(11, starship.getConsumables());
			pstmt.setString(12, Float.toString(starship.getHyperdriveRating()));
			pstmt.setString(13, Integer.toString(starship.getMGLT()));
			pstmt.setString(14, starship.getStarshipClass());


			// adds array values separated by commas
			StringBuilder sb = new StringBuilder("");
			if(! starship.getPilotsIds().isEmpty()){
				for(String res : starship.getPilotsIds()) {
					sb.append(res);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(15, sb.toString());

			sb = new StringBuilder("");
			if(! starship.getFilmsIds().isEmpty()){
				for(String film : starship.getFilmsIds()) {
					sb.append(film);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(16, sb.toString());

			pstmt.execute();

			pstmt.close();

		} catch (SQLException e) {
			System.err.println("Error adding starship to database\n");
			e.printStackTrace();
		}


	}

	/**
	 * Adds a species object correctly to database
	 * @param species
	 */
	public void addSpecies(Species species) {
		PreparedStatement pstmt;
		try {
			if(findObjectById(species.getId()) != null){
				System.out.println("User is already in db. Updating.\n");
				pstmt = con.prepareStatement("UPDATE species "
						+ "SET id=?, name=?, classification=?, designation=?, averageHeight=?, skinColors=?, hairColors=?, eyeColors=?, "
						+ "averageLifespan=?, homeworld=?, language=?, people=?, films=?"
						+ "WHERE id=?");
				pstmt.setString(14, species.getId());
			}

			else{
				pstmt = con.prepareStatement("INSERT INTO species "
						+ "(id, name, classification, designation, averageHeight, skinColors, hairColors, eyeColors, "
						+ "averageLifespan, homeworld, language, people, films)"
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
			}

			pstmt.setString(1, species.getId());
			pstmt.setString(2, species.getName());
			pstmt.setString(3, species.getClassification());
			pstmt.setString(4, species.getDesignation());
			pstmt.setString(5, Integer.toString(species.getAverageHeight()));
			pstmt.setString(6, species.getSkinColors());
			pstmt.setString(7, species.getHairColors());
			pstmt.setString(8, species.getEyeColors());
			pstmt.setString(9, Integer.toString(species.getAverageLifespan()));
			pstmt.setString(10, species.getHomeworldId());
			pstmt.setString(11, species.getLanguage());

			// adds array values separated by commas
			StringBuilder sb = new StringBuilder("");
			if(! species.getPeopleIds().isEmpty()){
				for(String res : species.getPeopleIds()) {
					sb.append(res);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(12, sb.toString());

			sb = new StringBuilder("");
			if(! species.getFilmsIds().isEmpty()){
				for(String film : species.getFilmsIds()) {
					sb.append(film);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(13, sb.toString());

			pstmt.execute();

			pstmt.close();

		} catch (SQLException e) {
			System.err.println("Error adding species to database\n");
			e.printStackTrace();
		}

	}

	/**
	 * Adds a vehicle object correctly to database
	 * @param vehicle
	 */
	public void addVehicle(Vehicle vehicle) {
		PreparedStatement pstmt;
		try {
			if(findObjectById(vehicle.getId()) != null){
				System.out.println("vehicle is already in db. Updating.\n");
				pstmt = con.prepareStatement("UPDATE vehicle "
						+ "SET id=?, name=?, model=?, manufacturer=?, cost=?, length=?,"
						+ "maxAtmospheringSpeed=?, crew=?, passengers=?, cargoCapacity=?, consumables=?,"
						+ "vehicleClass=?, pilots=?, films=?"
						+ "WHERE id=?");
				pstmt.setString(15, vehicle.getId());
			}

			else{
				pstmt = con.prepareStatement("INSERT INTO vehicle "
						+ "(id, name, model, manufacturer, cost, length,"
						+ "maxAtmospheringSpeed, crew, passengers, cargoCapacity, consumables,"
						+ "vehicleClass, pilots, films)"
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
			}

			pstmt.setString(1, vehicle.getId());
			pstmt.setString(2, vehicle.getName());
			pstmt.setString(3, vehicle.getModel());
			pstmt.setString(4, vehicle.getManufacturer());
			pstmt.setString(5, Double.toString(vehicle.getCost()));
			pstmt.setString(6, Double.toString(vehicle.getLength()));
			pstmt.setString(7, vehicle.getMaxAtmospheringSpeed());
			pstmt.setString(8, Integer.toString(vehicle.getCrew()));
			pstmt.setString(9, Integer.toString(vehicle.getPassengers()));
			pstmt.setString(10, Integer.toString(vehicle.getCargoCapacity()));
			pstmt.setString(11, vehicle.getConsumables());
			pstmt.setString(12, vehicle.getVehicleClass());

			// adds array values separated by commas
			StringBuilder sb = new StringBuilder("");
			if(! vehicle.getPilotsIds().isEmpty()){
				for(String res : vehicle.getPilotsIds()) {
					sb.append(res);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(13, sb.toString());

			sb = new StringBuilder("");
			if(! vehicle.getFilmsIds().isEmpty()){
				for(String film : vehicle.getFilmsIds()) {
					sb.append(film);
					sb.append(',');
				}
				sb.deleteCharAt(sb.length()-1);
			}
			pstmt.setString(14, sb.toString());

			pstmt.execute();

			pstmt.close();

		} catch (SQLException e) {
			System.err.println("Error adding vehicle to database\n");
			e.printStackTrace();
		}

	}

	/**
	 * Alters tables to give room for a picture url. 
	 * Default is empty string '', so the value does not interfere with existing functions. 
	 * This method should be run before database is used by an application
	 */
	public void addPictureUrlColumnToDatabase() {
		try {
			PreparedStatement pstmt = con.prepareStatement(
					"ALTER TABLE people \n"
					+ "ADD pictureUrl varchar(128) NOT NULL \n"
					+ "DEFAULT ''");

			pstmt.execute();
			
			pstmt.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Error altering table. \nThe altering might have been executed previously\n");
		}
	}
	

	/**
	 * Adds a url to the database for the object with id. 
	 * Should only add accepted urls created with HTMPParser-getPicture method.
	 * THIS METHOD CURRENTLY ONLY ADDS FOR A PEOPLE OBJECT and after addPictureUrlColumnToDatabase has been run
	 *  
	 * @param id of the object
	 * @param url accepted of the picture
	 */
	public void updateIdWithUrl(String id, String url){
		// checks that is people and addPictureUrlColumnToDatabase is run
		if(id.startsWith("people") && findObjectById(id).length == 15 ){
			try {
				PreparedStatement pstmt = con.prepareStatement("UPDATE people SET pictureUrl=? WHERE id=?");
				pstmt.setString(1, url);
				pstmt.setString(2, id);
				
				pstmt.execute();
				
				pstmt.close();
				
			} catch (SQLException e) {
				System.err.println("Error adding url to database\n");
				e.printStackTrace();
			}
			
			
		}
	}
	
	
	/**
	 * Gets a url for an object with the given id if it has been added and addPictureUrlColumnToDatabase has been run.
	 * ONLY WORKS FOR PEOPLE OBJECTS!
	 */
	public String getUrlFromId(String id){
		if(id.startsWith("people") && findObjectById(id).length == 15 ){
			try {
				PreparedStatement pstmt = con.prepareStatement("SELECT pictureUrl FROM people WHERE id=?");
				pstmt.setString(1, id);
				
				ResultSet res = pstmt.executeQuery(); 
				
				pstmt.close();
				
				if(res.next()){
					System.out.println("All good\n");
					return res.getString(1);
				}
			} catch (SQLException e) {
				System.err.println("Error fetching url to database\n");
				e.printStackTrace();
			}
		}
		return null;
	}
	
}
