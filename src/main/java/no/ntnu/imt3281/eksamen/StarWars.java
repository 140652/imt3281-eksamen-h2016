package no.ntnu.imt3281.eksamen;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import no.ntnu.imt3281.starWars.Film;
import no.ntnu.imt3281.starWars.People;

/**
 * @author amund
 * Class controls the StarWars.fxml loaded by App. 
 */
public class StarWars implements Initializable{
	
	@FXML
	private ComboBox<String> characterHomeworld;

	@FXML
	private ComboBox<String> films;

	@FXML
	private Label characterSkinColor;

	@FXML
	private Tab moviesTab;

	@FXML
    private Tab charactersTab;

	@FXML
    private TabPane tabPane;

	@FXML
	private Label releaseDate;

	@FXML
	private ComboBox<String> characterFilms;

	@FXML
	private Label director;

	@FXML
	private Label characterGender;

	@FXML
	private Label openingCrawl;

	@FXML
	private ComboBox<String> vehicles;

	@FXML
	private Label characterHairColor;

    @FXML
    private ImageView characterImage;

	@FXML
	private ComboBox<String> characterSpecies;

	@FXML
	private Label characterHeight;

	@FXML
	private ComboBox<String> characterStarships;

	@FXML
	private ComboBox<String> characters;

	@FXML
	private Label characterMass;

	@FXML
	private Label characterName;

	@FXML
	private ComboBox<String> planets;

	@FXML
	private ComboBox<String> starships;

	@FXML
	private ComboBox<String> species;

	@FXML
	private Label characterBirthYear;

	@FXML
	private Label producer;

	@FXML
	private Label characterEyeColor;

	@FXML
	private ComboBox<String> characterVehicles;
	
	// Containers for data loaded into memory
	private ArrayList<Film> loadedFilms;

	private ArrayList<People> loadedPeople;


	/**
	 * Overrides the initialize method run by javafx. 
	 * @param location 
	 * @param resources 
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// initializes variables for storing loaded data

		loadedFilms = new ArrayList<>();
		loadedPeople = new ArrayList<>();
		
		// Initializes the films titles from internet

		List<String> filmsJson = W3ApiController.getW3apicontroller().getTypeJson("films");
		System.out.println(filmsJson.toString());

		JSONParser parser = new JSONParser();
		Iterator<String> itr = filmsJson.iterator();
		while(itr.hasNext()){
			JSONObject obj;
			try {
				obj = (JSONObject) parser.parse(itr.next());
				films.getItems().add(obj.get("title").toString());
			} catch (ParseException e) {
				System.err.println("Error parsing film\n");
				e.printStackTrace();
			}
		}
	}

	/**
	 * When a film is clicked, gets and shows all data about that film 
	 * @param event
	 */
	@FXML
	void loadFilms(ActionEvent event) {
		Film theFilm = null;
		// if a value was chosen
		if(films.getValue() != null){
			String film = films.getValue();

			// searches memory for films
			for(Film loadedFilm : loadedFilms){
				if(loadedFilm.getName().equals(film)){
					theFilm = loadedFilm;
					break;
				}
			}

			// if film not found in last step
			if(theFilm == null){
				JSONParser parser = new JSONParser();

				List<String> filmsJson = W3ApiController.getW3apicontroller().getTypeJson("films");
				Iterator<String> itr = filmsJson.iterator();
				while(itr.hasNext()){
					JSONObject obj;

					try {

						obj = (JSONObject) parser.parse(itr.next());
						if(film.equals((obj.get("title").toString()))){
							theFilm = new Film(obj.get("url").toString().substring(20), obj.toJSONString());
							break;
						}

					} catch (ParseException e) {
						System.err.println("Error parsing film\n");
						e.printStackTrace();
					}
				}
			}
		}

		if(theFilm != null){
			loadedFilms.add(theFilm);
			showFilm(theFilm);
		}


	}


	/**
	 * Shows data about the film in question in app
	 * @param film to be shown
	 */
	private void showFilm(Film film) {

		releaseDate.setText(film.getReleaseDate());
		director.setText(film.getDirector());
		producer.setText(film.getProducer());
		openingCrawl.setText(film.getOpeningCrawl());
		setComboBoxFromArrayList(film.getCharactersIds(), characters);
		setComboBoxFromArrayList(film.getPlanetsIds(), planets);
		setComboBoxFromArrayList(film.getSpeciesIds(), species);
		setComboBoxFromArrayList(film.getVehiclesIds(), vehicles);
		setComboBoxFromArrayList(film.getStarshipsIds(), starships);

	}

	/**
	 * When characters combobox is clicked, shows data about a selected character in characters tab
	 * @param event
	 */
	@FXML
	void loadCharacter(ActionEvent event) {
		
		if(characters.getValue() != null){
			People thePeople = null;
			String selected = characters.getValue();
			tabPane.getSelectionModel().select(charactersTab);
			
			for(People person : loadedPeople){
				if(person.getName().equals(selected)){
					thePeople = person; 
					break;
				}
			}
			
			// if people was not loaded
			if(thePeople == null){
				JSONParser parser = new JSONParser();

				List<String> filmsJson = W3ApiController.getW3apicontroller().getTypeJson("people");
				Iterator<String> itr = filmsJson.iterator();
				while(itr.hasNext()){
					JSONObject obj;

					try {

						obj = (JSONObject) parser.parse(itr.next());
						if(selected.equals((obj.get("name").toString()))){
							thePeople = new People(obj.get("url").toString().substring(20), obj.toJSONString());
							break;
						}

					} catch (ParseException e) {
						System.err.println("Error parsing people\n");
						e.printStackTrace();
					}
				}				
			}
			
			if(thePeople != null) {
				loadedPeople.add(thePeople);
				showCharacter(thePeople);
			}
		}
	}

	/**
	 * Shows data in a People object in the Characters tab. Also tries to show image from star wars wikia.
	 * @param thePeople the people object to be displayed in the character tab
	 */
	private void showCharacter(People thePeople) {
		characterName.setText(thePeople.getName());
		
		if(thePeople.getHeight() == -1){
			characterHeight.setText("n/a");
		}else{
			characterHeight.setText(Integer.toString(thePeople.getHeight()));
		}
		
		if(Double.toString(thePeople.getMass()).equals("-1")){
			characterMass.setText("n/a");
		}else{
			characterMass.setText(Double.toString(thePeople.getMass()));
		}
		
		characterHairColor.setText(thePeople.getHairColor());
		characterSkinColor.setText(thePeople.getSkinColor());
		characterEyeColor.setText(thePeople.getEyeColor());
		characterBirthYear.setText(thePeople.getBirthYear());
		
		// punt the home world into an arraylist and sends to method
		ArrayList<String> lst = new ArrayList<>();
		lst.add(thePeople.getHomeworldId());
		setComboBoxFromArrayList(lst, characterHomeworld);

		setComboBoxFromArrayList(thePeople.getFilmsIds(), characterFilms);
		setComboBoxFromArrayList(thePeople.getStarshipsIds(), characterStarships);
		setComboBoxFromArrayList(thePeople.getVehiclesIds(), characterVehicles);
		setComboBoxFromArrayList(thePeople.getSpeciesIds(), characterSpecies);

		//search for url in database
		String imageUrl = DatabaseController.getController().getUrlFromId(thePeople.getId());
		
		// If not found in database
		if(imageUrl == null || imageUrl.equals("")){
			imageUrl = HTMLParser.getHtmlParser().getPicture(thePeople.getName());
		}
		
		// if url is found
		if(imageUrl != null && ! imageUrl.equals("")){
			characterImage.setImage(new Image(imageUrl));
			DatabaseController.getController().updateIdWithUrl(thePeople.getId(), imageUrl);
		}
		else{
			characterImage.setImage(null);
			System.out.println("No url found\n");
		}
	}

	/**
	 * Fills a combobox cb with names belonging to ids
	 * @param ids to be parsed
	 * @param cb combobox to be filled
	 */
	private void setComboBoxFromArrayList(ArrayList<String> ids, ComboBox<String> cb) {
		JSONParser parser = new JSONParser();
		// if no values, disables the combobox and sets to "n/a"
		if(ids == null) {
			cb.setDisable(true);
			cb.setValue("n/a");
			return;
		}
		// sets values in comboboxes
		for(String  id : ids){
			String text = "error";
			// searches db for character
			Object[] dbRes = DatabaseController.getController().findObjectById(id);
			if(dbRes != null){
				System.out.println(id + " was in db\n");
				text = (String) dbRes[1];
			}
			// not in db, finds json
			else{
				try {
					String json = W3ApiController.getW3apicontroller().getJsonForId(id);
					text = ((JSONObject) parser.parse(json)).get("name").toString();
				}

				catch (ParseException e) {
					System.err.println("Error parsing " + id + "\n");
					e.printStackTrace();
				}
			}
			cb.getItems().add(text);
		}
	}
}
