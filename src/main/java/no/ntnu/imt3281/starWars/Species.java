/**
 * 
 */
package no.ntnu.imt3281.starWars;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import no.ntnu.imt3281.eksamen.DatabaseController;
import no.ntnu.imt3281.eksamen.W3ApiController;

/**
 * @author amund
 * Class holds information about a species.
 */
public class Species {

	private String id;
	private String name;
	private String classification;
	private String designation;
	private int averageHeight;
	private String skinColors;
	private String hairColors;
	private String eyeColors;
	private int averageLifespan;
	private String homeworld;
	private String language;
	private ArrayList<String> people;
	private ArrayList<String> films;  


	/**
	 * Constructor takes an id and a json string with the data about a starship.
	 * @param id for the species
	 * @param json string with data about the species
	 */
	public Species(String id, String json) {
		this.id = id;
		parseJson(json);

		// adds this object to the database
		DatabaseController.getController().addSpecies(this);
	}

	/**
	 * @param json
	 */
	private void parseJson(String json) {
		JSONParser parser = new JSONParser();

		try {
			JSONObject object = (JSONObject) parser.parse(json);

			// finds the values by keys and casts them to correct type
			name = object.get("name").toString();
			classification = object.get("classification").toString();
			designation = object.get("designation").toString();
			if(object.get("average_height").toString().equals("n/a")){
				averageHeight = -1;
			}
			else{
				averageHeight = Integer.parseInt(object.get("average_height").toString());
			}
			skinColors = object.get("skin_colors").toString();
			hairColors = object.get("hair_colors").toString();
			eyeColors = object.get("eye_colors").toString();
			
			if(object.get("average_lifespan").toString().equals("unknown")
					|| object.get("average_lifespan").toString().equals("indefinite") ){
				averageLifespan = -1;
			}
			else{
				averageLifespan = Integer.parseInt(object.get("average_lifespan").toString());
			}
			
			if(object.get("homeworld") == null){
				homeworld = null;
			}
			else{
				homeworld = object.get("homeworld").toString().substring(20);
			}
			language = object.get("language").toString();


			people = null;
			JSONArray pepArr = (JSONArray) object.get("people");
			if(pepArr != null){
				people = new ArrayList<>();
				Iterator<String> itr = pepArr.iterator();
				while(itr.hasNext()){
					people.add(itr.next().substring(20));
				}
			}

			films = null;
			JSONArray filmsArr = (JSONArray) object.get("films");
			if(filmsArr != null){
				films = new ArrayList<>();
				Iterator<String> itr = filmsArr.iterator();
				while(itr.hasNext()){
					films.add(itr.next().substring(20));
				}
			}


		} catch (ParseException e) {
			System.err.println("Error parsing species json\n");
			e.printStackTrace();
		}
	}

	/**
	 * Looks for object in database, or fetches from swapi.co/api/
	 * @param id for the species
	 */
	public Species(String id) {
		Object[] dbData = DatabaseController.getController().findObjectById(id);
		if(dbData == null){
			System.out.println("Starship not in db\n");
			
			this.id = id;
			parseJson(W3ApiController.getW3apicontroller().getJsonForId(id));
			
			DatabaseController.getController().addSpecies(this); 
		}
		else{
			this.id = ((String) dbData[0]).trim();
			name = (String) dbData[1];
			classification = (String) dbData[2];
			designation = (String) dbData[3];
			averageHeight =  (int) dbData[4];
			skinColors = (String) dbData[5];
			hairColors = (String) dbData[6];
			eyeColors = (String) dbData[7];
			averageLifespan =  Integer.parseInt((String)dbData[8]);
			if(dbData[9] == null){
				homeworld = null;
			}
			else{
				homeworld = (String) dbData[9];
			}
			
			language = (String) dbData[10];
		
			// spilts strings on , and adds them to arraylists
			people = new ArrayList<>();
			String[] pepIds = dbData[11].toString().split(",");
			for(String pepId : pepIds){
				people.add(pepId);
			}

			films = new ArrayList<>();
			String[] filmsIds = dbData[12].toString().split(",");
			for(String filmId : filmsIds){
				films.add(filmId);
			}
		
		}
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @return the classification
	 */
	public String getClassification() {
		return classification;
	}


	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}


	/**
	 * @return the averageHeight
	 */
	public int getAverageHeight() {
		return averageHeight;
	}


	/**
	 * @return the skinColors
	 */
	public String getSkinColors() {
		return skinColors;
	}


	/**
	 * @return the hairColors
	 */
	public String getHairColors() {
		return hairColors;
	}


	/**
	 * @return the eyeColors
	 */
	public String getEyeColors() {
		return eyeColors;
	}


	/**
	 * @return the averageLifespan
	 */
	public int getAverageLifespan() {
		return averageLifespan;
	}


	/**
	 * @return the homeworldId
	 */
	public String getHomeworldId() {
		return homeworld;
	}
	
	/**
	 * @return the Homeworld object
	 */
	public Planet getHomeworld(){
		return new Planet(homeworld);
	}


	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}



	/**
	 * @return a People ArrayList
	 */
	public ArrayList<People> getPeople() {
		ArrayList<People> ret = new ArrayList<>();
		for(String res : people){
			ret.add(new People(res));
		}

		return ret;
	}

	/**
	 * @return the people ids
	 */
	public ArrayList<String> getPeopleIds() {
		return people;
	}

	/**
	 * Gets the ids of the Films
	 */
	public ArrayList<String> getFilmsIds(){
		return films; 
	}

	/**
	 * @return a Films ArrayList
	 */
	public ArrayList<Film> getFilms() {
		ArrayList<Film> ret = new ArrayList<>();
		for(String film : films){
			ret.add(new Film(film));
		}

		return ret;
	}

	/**
	 * Overrides the toString method from object. Returns name. 
	 */
	@Override
	public String toString(){
		return getName();
	}


	/**
	 * Checks if an object is equal to this SPECES
	 * @param candidate the object to be compared
	 * @return true if the ids are equal
	 */
	@Override 
	public boolean equals(Object candiadate){

		if(candiadate instanceof Species){
			Species c = (Species) candiadate;
			return c.getId().equals(getId());
		}
		return false;
	}

}
