package no.ntnu.imt3281.starWars;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class FilmTest {
	String json = "{\n"
			+"	\"title\": \"A New Hope\",\n"
			+"	\"episode_id\": 4,\n"
			+"	\"opening_crawl\": \"It is a period of civil war.\r\nRebel spaceships, striking\r\nfrom a hidden base, have won\r\ntheir first victory against\r\nthe evil Galactic Empire.\r\n\r\nDuring the battle, Rebel\r\nspies managed to steal secret\r\nplans to the Empire's\r\nultimate weapon, the DEATH\r\nSTAR, an armored space\r\nstation with enough power\r\nto destroy an entire planet.\r\n\r\nPursued by the Empire's\r\nsinister agents, Princess\r\nLeia races home aboard her\r\nstarship, custodian of the\r\nstolen plans that can save her\r\npeople and restore\r\nfreedom to the galaxy....\",\n"
			+"	\"director\": \"George Lucas\",\n"
			+"	\"producer\": \"Gary Kurtz, Rick McCallum\",\n"
			+"	\"release_date\": \"1977-05-25\",\n"
			+"	\"characters\": [\n"
			+"		\"http://swapi.co/api/people/1/\",\n"
			+"		\"http://swapi.co/api/people/2/\",\n"
			+"		\"http://swapi.co/api/people/3/\",\n"
			+"		\"http://swapi.co/api/people/4/\",\n"
			+"		\"http://swapi.co/api/people/5/\",\n"
			+"		\"http://swapi.co/api/people/6/\",\n"
			+"		\"http://swapi.co/api/people/7/\",\n"
			+"		\"http://swapi.co/api/people/8/\",\n"
			+"		\"http://swapi.co/api/people/9/\",\n"
			+"		\"http://swapi.co/api/people/10/\",\n"
			+"		\"http://swapi.co/api/people/12/\",\n"
			+"		\"http://swapi.co/api/people/13/\",\n"
			+"		\"http://swapi.co/api/people/14/\",\n"
			+"		\"http://swapi.co/api/people/15/\",\n"
			+"		\"http://swapi.co/api/people/16/\",\n"
			+"		\"http://swapi.co/api/people/18/\",\n"
			+"		\"http://swapi.co/api/people/19/\",\n"
			+"		\"http://swapi.co/api/people/81/\"\n"
			+"	],\n"
			+"	\"planets\": [\n"
			+"		\"http://swapi.co/api/planets/2/\",\n"
			+"		\"http://swapi.co/api/planets/3/\",\n"
			+"		\"http://swapi.co/api/planets/1/\"\n"
			+"	],\n"
			+"	\"starships\": [\n"
			+"		\"http://swapi.co/api/starships/2/\",\n"
			+"		\"http://swapi.co/api/starships/3/\",\n"
			+"		\"http://swapi.co/api/starships/5/\",\n"
			+"		\"http://swapi.co/api/starships/9/\",\n"
			+"		\"http://swapi.co/api/starships/10/\",\n"
			+"		\"http://swapi.co/api/starships/11/\",\n"
			+"		\"http://swapi.co/api/starships/12/\",\n"
			+"		\"http://swapi.co/api/starships/13/\"\n"
			+"	],\n"
			+"	\"vehicles\": [\n"
			+"		\"http://swapi.co/api/vehicles/4/\",\n"
			+"		\"http://swapi.co/api/vehicles/6/\",\n"
			+"		\"http://swapi.co/api/vehicles/7/\",\n"
			+"		\"http://swapi.co/api/vehicles/8/\"\n"
			+"	],\n"
			+"	\"species\": [\n"
			+"		\"http://swapi.co/api/species/5/\",\n"
			+"		\"http://swapi.co/api/species/3/\",\n"
			+"		\"http://swapi.co/api/species/2/\",\n"
			+"		\"http://swapi.co/api/species/1/\",\n"
			+"		\"http://swapi.co/api/species/4/\"\n"
			+"	],\n"
			+"	\"created\": \"2014-12-10T14:23:31.880000Z\",\n"
			+"	\"edited\": \"2015-04-11T09:46:52.774897Z\",\n"
			+"	\"url\": \"http://swapi.co/api/films/1/\"\n"
			+"}";
	
	@Test
	public void testConstructors() {
		// Create object with id and json string
		// Should create/update record in database.
		Film film = new Film("films/1/", json);	// ID and json string
		assertEquals(film.getId(), "films/1/");
		assertEquals(film.getName(), "A New Hope");
		assertEquals(film.getName(), film.getTitle());
		assertEquals(film.getName(), film.toString());
		assertEquals(film.getEpisodeId(), 4, 0);
		assertTrue(film.getOpeningCrawl() instanceof String);
		assertEquals(film.getDirector(), "George Lucas");
		assertEquals(film.getProducer(), "Gary Kurtz, Rick McCallum");
		assertEquals(film.getReleaseDate(), "1977-05-25");
		assertTrue(film.getCharacters() instanceof ArrayList<?>);
		assertTrue(film.getCharacters().get(0) instanceof People);
		assertEquals(film.getCharacters().size(), 18, 0);
		assertTrue(film.getPlanets() instanceof ArrayList<?>);
		assertTrue(film.getPlanets().get(0) instanceof Planet);
		assertEquals(film.getPlanets().size(), 3, 0);
		assertTrue(film.getStarships() instanceof ArrayList<?>);
		assertTrue(film.getStarships().get(0) instanceof Starship);
		assertEquals(film.getStarships().size(), 8, 0);
		assertTrue(film.getVehicles() instanceof ArrayList<?>);
		assertTrue(film.getVehicles().get(0) instanceof Vehicle);
		assertEquals(film.getVehicles().size(), 4, 0);
		assertTrue(film.getSpecies() instanceof ArrayList<?>);
		assertTrue(film.getSpecies().get(0) instanceof Species);
		assertEquals(film.getSpecies().size(), 5, 0);
		
		// Create object with id only.
		// Should read record from database if exists or from 
		// network if not exists. Since we just added it above it should be in the database.
		Film aNewHope = new Film("films/1/");	// ID and json string
		assertEquals(film, aNewHope);			// Must override equals in Film
	}

}
