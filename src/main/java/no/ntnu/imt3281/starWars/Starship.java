/**
 * 
 */
package no.ntnu.imt3281.starWars;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import no.ntnu.imt3281.eksamen.DatabaseController;
import no.ntnu.imt3281.eksamen.W3ApiController;

/**
 * @author amund
 * Class holds information about a planet.
 */
public class Starship {

	private String id;
	private String name;
	private String model;
	private String manufacturer;
	private double cost;
	private float length;
	private String maxAtmospheringSpeed;
	private int crew;
	private int passengers;
	private long cargoCapacity;
	private String consumables;
	private float hyperdriveRating;
	private int MGLT;
	private String starshipClass;
	private ArrayList<String> pilots;
	private ArrayList<String> films;
	
	/**
	 * Constructor takes an id and a json string with the data about a starship.
	 * @param id for the starship
	 * @param json string with data about the starship
	 */
	public Starship(String id, String json) {
		this.id = id;
		parseJson(json);

		DatabaseController.getController().addStarship(this);

	}

	/**
	 * @param json to be parsed to this object
	 */
	private void parseJson(String json) {
		JSONParser parser = new JSONParser();

		try {
			JSONObject object = (JSONObject) parser.parse(json);

			name = object.get("name").toString();
			model = (String) object.get("model");
			manufacturer = (String) object.get("manufacturer");
			
			if(object.get("cost_in_credits").toString().equals("unknown")){
				cost = -1;
			}
			else{
				cost = Double.parseDouble((String) object.get("cost_in_credits"));
			}
			
			length = Float.parseFloat((String)object.get("length").toString().replaceAll(",", ""));
			maxAtmospheringSpeed = (String) object.get("max_atmosphering_speed");
			crew = Integer.parseInt((String) object.get("crew"));
			passengers = Integer.parseInt((String) object.get("passengers"));
			cargoCapacity = Long.parseLong((String) object.get("cargo_capacity"));
			consumables = (String) object.get("consumables");
			hyperdriveRating = Float.parseFloat((String)object.get("hyperdrive_rating"));
			MGLT = Integer.parseInt((String) object.get("MGLT"));
			starshipClass = (String) object.get("starship_class");
			
			pilots = null;
			JSONArray resArr = (JSONArray) object.get("pilots");
			if(resArr != null){
				pilots = new ArrayList<>();
				Iterator<String> itr = resArr.iterator();
				while(itr.hasNext()){
					pilots.add(itr.next().substring(20));
				}
			}

			films = null;
			JSONArray filmsArr = (JSONArray) object.get("films");
			if(filmsArr != null){
				films = new ArrayList<>();
				Iterator<String> itr = filmsArr.iterator();
				while(itr.hasNext()){
					films.add(itr.next().substring(20));
				}
			}

		} catch (ParseException e) {
			System.err.println("Error parsing starship json\n");
			e.printStackTrace();
		}
	}

	/**
	 * Looks for object in database, or fetches from swapi.co/api/
	 * @param id for the starship
	 */
	public Starship(String id) {
		Object[] dbData = DatabaseController.getController().findObjectById(id);
		if(dbData == null){
			System.out.println("Starship not in db\n");
			
			this.id = id;
			parseJson(W3ApiController.getW3apicontroller().getJsonForId(id));
			
			DatabaseController.getController().addStarship(this);
		}
		else{
			this.id = ((String) dbData[0]).trim();
			name = (String) dbData[1];
			model = (String) dbData[2];
			manufacturer = (String) dbData[3];
			cost = (double) dbData[4];
			length = Float.parseFloat(dbData[5].toString());
			maxAtmospheringSpeed = (String) dbData[6];
			crew = (int) dbData[7];
			passengers = (int) dbData[8];
			cargoCapacity = (long) dbData[9];
			consumables = (String) dbData[10];
			hyperdriveRating = Float.parseFloat(dbData[11].toString());
			MGLT = (int) dbData[12];
			starshipClass = (String) dbData[13];
			
			
			// spilts strings on , and adds them to arraylists
			pilots = new ArrayList<>();
			String[] pilIds = dbData[14].toString().split(",");
			for(String pilId : pilIds){
				pilots.add(pilId);
			}

			films = new ArrayList<>();
			String[] filmsIds = dbData[15].toString().split(",");
			for(String filmId : filmsIds){
				films.add(filmId);
			}

		}
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @return the manufacturer
	 */
	public String getManufacturer() {
		return manufacturer;
	}

	/**
	 * @return the cost
	 */
	public double getCost() {
		return cost;
	}

	/**
	 * @return the length
	 */
	public float getLength() {
		return length;
	}

	/**
	 * @return the maxAtmospheringSpeed
	 */
	public String getMaxAtmospheringSpeed() {
		return maxAtmospheringSpeed;
	}

	/**
	 * @return the crew
	 */
	public int getCrew() {
		return crew;
	}

	/**
	 * @return the passengers
	 */
	public int getPassengers() {
		return passengers;
	}

	/**
	 * @return the cargoCapacity
	 */
	public long getCargoCapacity() {
		return cargoCapacity;
	}

	/**
	 * @return the consumables
	 */
	public String getConsumables() {
		return consumables;
	}

	/**
	 * @return the hyperdriveRating
	 */
	public float getHyperdriveRating() {
		return hyperdriveRating;
	}

	/**
	 * @return the mGLT
	 */
	public int getMGLT() {
		return MGLT;
	}

	/**
	 * @return the starshipClass
	 */
	public String getStarshipClass() {
		return starshipClass;
	}


	/**
	 * @return a People ArrayList
	 */
	public ArrayList<People> getPilots() {
		ArrayList<People> ret = new ArrayList<>();
		for(String res : pilots){
			ret.add(new People(res));
		}

		return ret;
	}

	/**
	 * @return the Pilots ids
	 */
	public ArrayList<String> getPilotsIds() {
		return pilots;
	}

	/**
	 * Gets the ids of the Films
	 */
	public ArrayList<String> getFilmsIds(){
		return films; 
	}

	/**
	 * @return a Films ArrayList
	 */
	public ArrayList<Film> getFilms() {
		ArrayList<Film> ret = new ArrayList<>();
		for(String film : films){
			ret.add(new Film(film));
		}

		return ret;
	}

	/**
	 * Overrides the toString method from object. Returns name. 
	 */
	@Override
	public String toString(){
		return getName();
	}


	/**
	 * Checks if an object is equal to this starship
	 * @param candidate the object to be compared
	 * @return true if the ids are equal
	 */
	@Override 
	public boolean equals(Object candiadate){

		if(candiadate instanceof Starship){
			Starship c = (Starship) candiadate;
			return c.getId().equals(getId());
		}
		return false;
	}

}
