/**
 * 
 */
package no.ntnu.imt3281.eksamen;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * @author amund
 * Singleton class fetches pictures from starwars wikia.
 */
public class HTMLParser {

	// initiate singleton
	private static final HTMLParser htmlParser = new HTMLParser();
	
	
	/**
	 * @return the htmlParser
	 */
	public static HTMLParser getHtmlParser() {
		return htmlParser;
	}


	/**
	 * Method takes a name and searches for a fitting picture on http://starwars.wikia.com/wiki/
	 * @param name of star wars related object to be pictured
	 */
	public String getPicture(String name){
		
		try {
			String urlEnding = name.trim().replaceAll(" ", "_");
			Document doc = Jsoup.connect("http://starwars.wikia.com/wiki/" + urlEnding).get();
			Element e = doc.select("#WikiaMainContent aside figure a img").first();
			return e.attr("src");
		} catch (IOException e) {
			System.err.println("Error loading picture for " + name + '\n');
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * Calls method in DatabaseController that adds a pictureurl column to the database.
	 * @param args
	 */
	public static void main(String[] args){
		// lets the databasecontroller alter table
		DatabaseController.getController().addPictureUrlColumnToDatabase();
	}
}
