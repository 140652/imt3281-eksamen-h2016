package no.ntnu.imt3281.starWars;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class SpeciesTest {
	String json = "{"
			+"	\"name\": \"Hutt\","
			+"	\"classification\": \"gastropod\","
			+"	\"designation\": \"sentient\","
			+"	\"average_height\": \"300\","
			+"	\"skin_colors\": \"green, brown, tan\","
			+"	\"hair_colors\": \"n/a\","
			+"	\"eye_colors\": \"yellow, red\","
			+"	\"average_lifespan\": \"1000\","
			+"	\"homeworld\": \"http://swapi.co/api/planets/24/\","
			+"	\"language\": \"Huttese\","
			+"	\"people\": ["
			+"	\"http://swapi.co/api/people/16/\""
			+"	],"
			+"	\"films\": ["
			+"	\"http://swapi.co/api/films/3/\","
			+"	\"http://swapi.co/api/films/1/\""
			+"	],"
			+"	\"created\": \"2014-12-10T17:12:50.410000Z\","
			+"	\"edited\": \"2014-12-20T21:36:42.146000Z\","
			+"	\"url\": \"http://swapi.co/api/species/5/\""
			+ "}";
	
	@Test
	public void testConstructors() {
		// Create object with id and json string
		// Should create/update record in database.
		Species species = new Species("species/5/", json);
		assertEquals(species.getId(), "species/5/");
		assertEquals(species.getName(), "Hutt");
		assertEquals(species.getName(), species.toString());
		assertEquals(species.getClassification(), "gastropod");
		assertEquals(species.getDesignation(), "sentient");
		assertEquals(species.getAverageHeight(), 300, 0);
		assertEquals(species.getSkinColors(), "green, brown, tan");
		assertEquals(species.getHairColors(), "n/a");
		assertEquals(species.getEyeColors(), "yellow, red");
		assertEquals(species.getAverageLifespan(), 1000, 0);
		assertTrue(species.getHomeworld() instanceof Planet);
		assertEquals(species.getLanguage(), "Huttese");
		assertTrue(species.getPeople() instanceof ArrayList<?>);
		assertTrue(species.getPeople().get(0) instanceof People);
		assertEquals(species.getPeople().size(), 1, 0);
		assertTrue(species.getFilms() instanceof ArrayList<?>);
		assertTrue(species.getFilms().get(0) instanceof Film);
		assertEquals(species.getFilms().size(), 2, 0);
		
		// Create object with id only.
		// Should read record from database if exists or from 
		// network if not exists. Since we just added it above it should be in the database.
		Species hutt = new Species("species/5/");	// ID and json string
		assertEquals(species, hutt);				// Must override equals in species
	}
}
