/**
 * 
 */
package no.ntnu.imt3281.starWars;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import no.ntnu.imt3281.eksamen.DatabaseController;
import no.ntnu.imt3281.eksamen.W3ApiController;

/**
 * @author amund
 * Class holds information about a film.
 */
public class Film {

	private String id;
	private String title;
	private int episodeId;
	private String openingCrawl;
	private String director;
	private String producer;
	private String releaseDate;
	private ArrayList<String> characters;
	private ArrayList<String> planets;
	private ArrayList<String> starships;
	private ArrayList<String> vehicles;
	private ArrayList<String> species;
	
	
	/**
	 * Constructor takes an id and a json string with the data about a Film. 
	 * @param id of the film
	 * @param json with data
	 */
	public Film(String id, String json) {
		this.id = id;
		parseJson(json);
		
		DatabaseController.getController().addFilm(this);

	}

	/**
	 * @param json
	 */
	private void parseJson(String json) {
		JSONParser parser = new JSONParser();

		try {
			JSONObject object = (JSONObject) parser.parse(json);
			
			title = object.get("title").toString();
			episodeId = Integer.parseInt(object.get("episode_id").toString());
			openingCrawl = object.get("opening_crawl").toString();
			director = object.get("director").toString();
			producer = object.get("producer").toString();
			releaseDate = object.get("release_date").toString();
			
			// parse charaters
			characters = null;
			JSONArray charArr = (JSONArray) object.get("characters");
			if(charArr != null){
				characters = new ArrayList<>();
				Iterator<String> itr = charArr.iterator();
				while(itr.hasNext()){
					characters.add(itr.next().substring(20));
				}
			}
			
			// parse planets
			planets = null;
			JSONArray planetsArr = (JSONArray) object.get("planets");
			if(planetsArr != null){
				planets = new ArrayList<>();
				Iterator<String> itr = planetsArr.iterator();
				while(itr.hasNext()){
					planets.add(itr.next().substring(20));
				}
			}

			// parse species
			species = null;
			JSONArray speciesArr = (JSONArray) object.get("species");
			if(speciesArr != null){
				species = new ArrayList<>();
				Iterator<String> itr = speciesArr.iterator();
				while(itr.hasNext()){
					species.add(itr.next().substring(20));
				}
			}

			// parse vehicles
			vehicles = null;
			JSONArray vehiclesArr = (JSONArray) object.get("vehicles");
			if(vehiclesArr != null){
				vehicles = new ArrayList<>();
				Iterator<String> itr = vehiclesArr.iterator();
				while(itr.hasNext()){
					vehicles.add(itr.next().substring(20));
				}
			}

			// parse starships
			starships = null;
			JSONArray starshipsArr = (JSONArray) object.get("starships");
			if(starshipsArr != null){
				starships = new ArrayList<>();
				Iterator<String> itr = starshipsArr.iterator();
				while (itr.hasNext()){
					starships.add(itr.next().substring(20));
				}
			}
			
			

			
		} catch (ParseException e) {
			System.err.println("Error parsing film json\n");
			e.printStackTrace();
		}
	}

	/**
	 * Looks for object in database, or fetches from swapi.co/api/
	 * @param id for the film
	 */
	public Film(String id) {
		Object[] dbData = DatabaseController.getController().findObjectById(id);
		if(dbData == null){
			System.out.println("Film not in db\n");
			this.id = id;
			parseJson(W3ApiController.getW3apicontroller().getJsonForId(id));
			
			DatabaseController.getController().addFilm(this);
		}
		else{
			this.id = ((String) dbData[0]).trim();
			title = (String) dbData[1];
			episodeId = (int) dbData[2];
			openingCrawl = (String) dbData[3];
			director = (String) dbData[4];
			producer = (String) dbData[5];
			releaseDate = (String) dbData[6];
			
			// spilts strings on , and adds them to arraylists
			characters = new ArrayList<>();
			String[] charIds = dbData[7].toString().split(",");
			for(String charId : charIds){
				characters.add(charId);
			}
			
			
			planets = new ArrayList<>();
			String[] planetsIds = dbData[10].toString().split(",");
			for(String planetId : planetsIds){
				planets.add(planetId);
			}
			
			
			starships = new ArrayList<>();
			String[] starshipIds = dbData[10].toString().split(",");
			for(String starshipId : starshipIds){
				starships.add(starshipId);
			}
			
			vehicles = new ArrayList<>();
			String[] vehicleIds = dbData[10].toString().split(",");
			for(String vehicleId : vehicleIds){
				vehicles.add(vehicleId);
			}
			
			species = new ArrayList<>();
			String[] speciesIds = dbData[10].toString().split(",");
			for(String speciesId : speciesIds){
				species.add(speciesId);
			}
			
		}
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}


	/**
	 * @return the episodeId
	 */
	public int getEpisodeId() {
		return episodeId;
	}


	/**
	 * @return the openingCrawl
	 */
	public String getOpeningCrawl() {
		return openingCrawl;
	}


	/**
	 * @return the director
	 */
	public String getDirector() {
		return director;
	}


	/**
	 * @return the producer
	 */
	public String getProducer() {
		return producer;
	}


	/**
	 * @return the releaseDate
	 */
	public String getReleaseDate() {
		return releaseDate;
	}


	/**
	 * @return a People ArrayList
	 */
	public ArrayList<People> getCharacters() {
		ArrayList<People> ret = new ArrayList<>();
		for(String pep : characters){
			ret.add(new People(pep));
		}

		return ret;
	}


	/**
	 * Returns ids of characters
	 * @return
	 */
	public ArrayList<String> getCharactersIds() {
		return characters;
	}
	
	
	/**
	 * @return a Planets ArrayList
	 */
	public ArrayList<Planet> getPlanets() {
		ArrayList<Planet> ret = new ArrayList<>();
		for(String planet : planets){
			ret.add(new Planet(planet));
		}
		
		return ret;
	}
	
	/**
	 * Returns ids of planets
	 * @return
	 */
	public ArrayList<String> getPlanetsIds() {
		return planets;
	}


	/**
	 * @return a Starships ArrayList
	 */
	public ArrayList<Starship> getStarships() {
		ArrayList<Starship> ret = new ArrayList<>();
		for(String starship : starships){
			ret.add(new Starship(starship));
		}

		return ret;
	}

	
	/**
	 * Returns ids of starships
	 * @return
	 */
	public ArrayList<String> getStarshipsIds() {
		return starships;
	}

	
	/**
	 * @return a Vehicle ArrayList
	 */
	public ArrayList<Vehicle> getVehicles() {
		ArrayList<Vehicle> ret = new ArrayList<>();
		for(String vehicle : vehicles){
			ret.add(new Vehicle(vehicle));
		}

		return ret;
	}

	/**
	 * Returns ids of vehicles
	 * @return
	 */
	public ArrayList<String> getVehiclesIds() {
		return vehicles;
	}
	
	/**
	 * @return a Species ArrayList
	 */
	public ArrayList<Species> getSpecies() {
		ArrayList<Species> ret = new ArrayList<>();
		for(String specie : species){
			ret.add(new Species(specie));
		}
		
		return ret;
	}


	/**
	 * Returns ids of Species
	 * @return
	 */
	public ArrayList<String> getSpeciesIds() {
		return species;
	}
	
	
	/**
	 * @return name is same as title 
	 */
	public String getName() {
		return title;
	}

	/**
	 * Overrides the toString method from object. Returns name. 
	 */
	@Override
	public String toString(){
		return getName();
	}
	
	
	/**
	 * Checks if an object is equal to this film
	 * @param comp the object to be compared
	 * @return true if equal
	 */
	@Override 
	public boolean equals(Object comp){
		
		if(comp instanceof Film){
			Film c = (Film) comp;
			return c.getId().equals(getId());
		}
		return false;
	}

}
