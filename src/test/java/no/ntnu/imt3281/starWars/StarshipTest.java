package no.ntnu.imt3281.starWars;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class StarshipTest {
	String json = "{"
			+"	\"name\": \"Star Destroyer\","
			+"	\"model\": \"Imperial I-class Star Destroyer\","
			+"	\"manufacturer\": \"Kuat Drive Yards\","
			+"	\"cost_in_credits\": \"150000000\","
			+"	\"length\": \"1,600\","
			+"	\"max_atmosphering_speed\": \"975\","
			+"	\"crew\": \"47060\","
			+"	\"passengers\": \"0\","
			+"	\"cargo_capacity\": \"36000000\","
			+"	\"consumables\": \"2 years\","
			+"	\"hyperdrive_rating\": \"2.0\","
			+"	\"MGLT\": \"60\","
			+"	\"starship_class\": \"Star Destroyer\","
			+"	\"pilots\": [],"
			+"	\"films\": ["
			+"		\"http://swapi.co/api/films/3/\","
			+"		\"http://swapi.co/api/films/2/\","
			+"		\"http://swapi.co/api/films/1/\""
			+"	],"
			+"	\"created\": \"2014-12-10T15:08:19.848000Z\","
			+"	\"edited\": \"2014-12-22T17:35:44.410941Z\","
			+"	\"url\": \"http://swapi.co/api/starships/3/\""
			+"}";

	@Test
	public void testConstructors() {
		// Create object with id and json string
		// Should create/update record in database.
		Starship starship = new Starship("starships/3/", json);
		assertEquals(starship.getId(), "starships/3/");
		assertEquals(starship.getName(), "Star Destroyer");
		assertEquals(starship.getName(), starship.toString());
		assertEquals(starship.getModel(), "Imperial I-class Star Destroyer");
		assertEquals(starship.getManufacturer(), "Kuat Drive Yards");
		assertEquals(starship.getCost(), 150000000, 0);
		assertEquals(starship.getLength(), 1600, 0);
		assertEquals(starship.getMaxAtmospheringSpeed(), "975");
		assertEquals(starship.getCrew(), 47060, 0);
		assertEquals(starship.getPassengers(), 0, 0);
		assertEquals(starship.getCargoCapacity(), 36000000, 0);
		assertEquals(starship.getConsumables(), "2 years");
		assertEquals(starship.getHyperdriveRating(), 2, 0);
		assertEquals(starship.getMGLT(), 60, 0);
		assertEquals(starship.getStarshipClass(), "Star Destroyer");
		assertTrue(starship.getPilots() instanceof ArrayList<?>);
		assertEquals(starship.getPilots().size(), 0, 0);
		assertTrue(starship.getFilms() instanceof ArrayList<?>);
		assertTrue(starship.getFilms().get(0) instanceof Film);
		assertEquals(starship.getFilms().size(), 3, 0);
		
		// Create object with id only.
		// Should read record from database if exists or from 
		// network if not exists. Since we just added it above it should be in the database.
		Starship starDestroyer = new Starship("starships/3/");	// ID and json string
		assertEquals(starship, starDestroyer);					// Must override equals in Starship
	}
		
}
