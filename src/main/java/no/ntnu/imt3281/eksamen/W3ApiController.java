/**
 * 
 */
package no.ntnu.imt3281.eksamen;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author amund
 * Class is a singleton. Connects to http://swapi.co/api/ and fetches json 
 */
public class W3ApiController {

	private static final W3ApiController w3ApiController = new W3ApiController();

	private String allFilmsJson;
	
	
	private W3ApiController(){
		allFilmsJson = getJsonForId("films");
	}
	
	
	/**
	 * @return the w3apicontroller
	 */
	public static W3ApiController getW3apicontroller() {
		return w3ApiController;
	}

	/**
	 * Searches the url http://swapi.co/api/[parameter value] and returns json string if found
	 * @param id for the sought object
	 * source: Pavel's answer at: http://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string#5445161
	 */
	public String getJsonForId(String id) {
		 
		String jsonString = "";
		try {
			URL url = new URL ("http://swapi.co/api/" + id);
			URLConnection connection = url.openConnection();
			// Must set user-agent, the java default user agent is denied
			connection.setRequestProperty("User-Agent", "curl/7.8 (i386-redhat-linux-gnu) libcurl 7.8 (OpenSSL 0.9.6b) (ipv6 enabled)");
			// Must set accept to application/json, if not html is returned
			connection.setRequestProperty("Accept", "application/json");
			connection.connect();
			// Read from this input stream
			InputStream is = connection.getInputStream();
			
			JSONParser parser = new JSONParser();
			
			Scanner sc = new Scanner(is);
			String result = sc.nextLine();
			sc.close();
			
			// Read and parse the response
	    	JSONObject obj = (JSONObject) parser.parse(result);
	    	jsonString = obj.toJSONString();
			
		} catch (MalformedURLException e) {
			System.err.println("Error fetching url\n");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Error reading from input stream\n");
			e.printStackTrace();
		} catch (ParseException e) {
			System.err.println("Error parsing string\n");
			e.printStackTrace();
		}
		
		return jsonString;
	}
	
	
	/**
	 * Gets the json for a type in the database, and returns as a List of Strings
	 * @param type 
	 */
	public List<String> getTypeJson(String type)	{
		List<String> jsonStrings = new ArrayList<>();
		JSONParser parser = new JSONParser();
		String json;
		
		// films is loaded by constructor
		if(type.equals("films")){
			json = allFilmsJson;
		}
		else{
			json = getJsonForId(type);
		}
		
		try {
			JSONObject obj = (JSONObject) parser.parse(json);
			JSONArray titleArray = (JSONArray) obj.get("results");
			Iterator<JSONObject> itr = titleArray.iterator();
			while(itr.hasNext()){
				jsonStrings.add(itr.next().toString());
			}
			
		} catch (ParseException e) {
			System.err.println("Error parsing " + type + "\n");
			e.printStackTrace();
		}
		
		return jsonStrings;
	}
}
