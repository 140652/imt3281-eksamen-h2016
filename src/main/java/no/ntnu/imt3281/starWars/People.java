/**
 * 
 */
package no.ntnu.imt3281.starWars;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import no.ntnu.imt3281.eksamen.DatabaseController;
import no.ntnu.imt3281.eksamen.W3ApiController;

/**
 * @author amund
 * Class holds information about a people (person?). 
 */
public class People {

	private String id;
	private String name; 
	private int height;
	private double mass;
	private String hairColor;
	private String skinColor;
	private String eyeColor;
	private String birthYear;
	private String gender;
	private String homeworld;
	private ArrayList<String> films;
	private ArrayList<String> species;
	private ArrayList<String> vehicles;
	private ArrayList<String> starships;
	
	
	/**
	 * Takes an id and a string with the data. Parses the json and adds it to the database.
	 * @param id for the people
	 * @param json with data about the people
	 */
	public People(String id, String json) {
		this.id = id;
		parseJson(json);
		
		// adds this object to the database
		DatabaseController.getController().addPeopleToDatabase(this);
		
	}
	
	/**
	 * Looks for object in database, or fetches from swapi.co/api/
	 * @param id for the people
	 */
	public People(String id) {
		Object[] dbData = DatabaseController.getController().findObjectById(id);
		if(dbData == null){
			System.out.println("People not in db\n");
			
			this.id = id;
			parseJson(W3ApiController.getW3apicontroller().getJsonForId(id));
			
			DatabaseController.getController().addPeopleToDatabase(this);
		}
		else{
			this.id = ((String) dbData[0]).trim();
			name = (String) dbData[1];
			height = (int) dbData[2];
			mass = (double) dbData[3];
			hairColor = (String) dbData[4];
			skinColor = (String) dbData[5];
			eyeColor = (String) dbData[6];
			birthYear = (String) dbData[7];
			gender = (String) dbData[8];
			homeworld = (String) dbData[9];
			
			films = new ArrayList<>();
			// splits string on , and creates data from the ids. 
			String[] filmIds = dbData[10].toString().split(",");
			for(String filmId : filmIds){
				films.add(filmId);
			}
			
			species = new ArrayList<>();
			String[] speciesIds = dbData[11].toString().split(",");
			for(String speciesId : speciesIds){
				species.add(speciesId);
			}
			
			vehicles = new ArrayList<>();
			String[] vehicleIds = dbData[12].toString().split(",");
			for(String vehicleId : vehicleIds){
				vehicles.add(vehicleId);
			}
			
			starships = new ArrayList<>();
			String[] starshipIds = dbData[13].toString().split(",");
			for(String starshipId : starshipIds){
				starships.add(starshipId);
			}
			
		}
	}

	/**
	 * @param json
	 */
	private void parseJson(String json) {
		JSONParser parser = new JSONParser();
		
		try {
			JSONObject object = (JSONObject) parser.parse(json);
			
			// finds the values by keys and casts them to correct type
			name = object.get("name").toString();
			if(object.get("height").toString().equals("unknown")){
				height = -1;
			}
			height = Integer.parseInt(object.get("height").toString().replaceAll(",", ""));
			if(object.get("mass").toString().equals("unknown")){
				mass = -1;
			}
			else{
				mass = Float.parseFloat(object.get("mass").toString().replaceAll(",", ""));
			}
			hairColor = object.get("hair_color").toString();
			skinColor = object.get("skin_color").toString();
			eyeColor = object.get("eye_color").toString();
			birthYear = object.get("birth_year").toString();
			gender = object.get("gender").toString();
			
			// parse homeworld
			homeworld = object.get("homeworld").toString().substring(20);
			
			// parse films
			films = null;
			JSONArray filmsArr = (JSONArray) object.get("films");
			if(filmsArr != null){
				films = new ArrayList<>();
				Iterator<String> itr = filmsArr.iterator();
				while(itr.hasNext()){
					films.add(itr.next().substring(20));
				}
			}
			
			// parse species
			species = null;
			JSONArray speciesArr = (JSONArray) object.get("species");
			if(speciesArr != null){
				species = new ArrayList<>();
				Iterator<String> itr = speciesArr.iterator();
				while(itr.hasNext()){
					species.add(itr.next().substring(20));
				}
			}
			
			// parse vehicles
			vehicles = null;
			JSONArray vehiclesArr = (JSONArray) object.get("vehicles");
			if(vehiclesArr != null){
				vehicles = new ArrayList<>();
				Iterator<String> itr = vehiclesArr.iterator();
				while(itr.hasNext()){
					vehicles.add(itr.next().substring(20));
				}
			}
			
			// parse starships
			starships = null;
			JSONArray starshipsArr = (JSONArray) object.get("starships");
			if(starshipsArr != null){
				starships = new ArrayList<>();
				Iterator<String> itr = starshipsArr.iterator();
				while (itr.hasNext()){
					starships.add(itr.next().substring(20));
				}
			}
			
		} catch (ParseException e) {
			System.err.println("Error parsing people json\n");
			e.printStackTrace();
		}
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @return the height. -1 if unknown
	 */
	public int getHeight() {
		return height;
	}


	/**
	 * @return the mass. -1 if unknown
	 */
	public double getMass() {
		return mass;
	}


	/**
	 * @return the hairColor
	 */
	public String getHairColor() {
		return hairColor;
	}


	/**
	 * @return the skinCOlor
	 */
	public String getSkinColor() {
		return skinColor;
	}


	/**
	 * @return the eyeColor
	 */
	public String getEyeColor() {
		return eyeColor;
	}


	/**
	 * @return the birthYear
	 */
	public String getBirthYear() {
		return birthYear;
	}


	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}


	/**
	 * @return the homeworld object
	 */
	public Planet getHomeworld() {
		return new Planet(homeworld);
	}
	
	/**
	 * Gets the id of HomeWorld
	 */
	public String getHomeworldId(){
		return homeworld;
	}
	
	/**
	 * Gets the ids of the Films
	 */
	public ArrayList<String> getFilmsIds(){
		return films; 
	}

	/**
	 * @return a Films ArrayList
	 */
	public ArrayList<Film> getFilms() {
		ArrayList<Film> ret = new ArrayList<>();
		for(String film : films){
			ret.add(new Film(film));
		}
		
		return ret;
	}


	/**
	 * @return a Species ArrayList
	 */
	public ArrayList<Species> getSpecies() {
		ArrayList<Species> ret = new ArrayList<>();
		for(String specie : species){
			ret.add(new Species(specie));
		}
		
		return ret;
	}

	/**
	 * Gets the ids of the species
	 */
	public ArrayList<String> getSpeciesIds(){
		return species; 
	}

	/**
	 * @return the Vehicles ArrayList
	 */
	public ArrayList<Vehicle> getVehicles() {
		ArrayList<Vehicle> ret = new ArrayList<>();
		for(String vehicle : vehicles){
			ret.add(new Vehicle(vehicle));
		}
		
		return ret;
	}

	/**
	 * Gets the ids of the vehicles
	 */
	public ArrayList<String> getVehiclesIds(){
		return vehicles; 
	}

	/**
	 * @return a Starships ArrayList
	 */
	public ArrayList<Starship> getStarships() {
		ArrayList<Starship> ret = new ArrayList<>();
		for(String starship : starships){
			ret.add(new Starship(starship));
		}
		
		return ret;
	}
	
	/**
	 * Gets the ids of the starships
	 */
	public ArrayList<String> getStarshipsIds(){
		return starships;
	}
	
	
	/**
	 * Overrides the toString method from object. Returns name. 
	 */
	@Override
	public String toString(){
		return getName();
	}
	
	
	/**
	 * Checks if an object is equal to this people
	 * @param comp the object to be compared
	 * @return
	 */
	@Override 
	public boolean equals(Object comp){
		if(comp instanceof People){
			People c = (People) comp;
			// TODO: Check the arraylists differently
			return c.getId().equals(getId()) && c.getName().equals(getName());
		}
		
		return false;
	}

}
